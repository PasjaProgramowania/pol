POL++ jest systemem edukacji, mającym na celu w sposób prosty i przyjemny przybliżyć użytkownikowi fundamenty programowania wspólne dla większości języków. Składnia programistyczna POL++ bazuje na C++, ale została zastąpiona polskimi wyrazami, tak aby użytkownik w pierwszych krokach nauki mógł swobodnie zrozumieć i wykorzystywać najważniejsze instrukcje używane w procesie tworzenia aplikacji. 

Jednocześnie ukończenie całego programu edukacji umożliwia bezpośrednie przejście do programowania w języku C++, jak i innych języków. Wykorzystane zostało zmodyfikowane środowisko pracy Code::Blocks, którego używa na codzień spora część studentów informatyki.

POL++ jest przeznaczony do celów edukacyjnych zarówno dla studentów, którzy wcześniej nie mieli nic wspólnego z programowaniem, jak i edukacji dzieci od 12 roku życia. Dodatkowo w celu uproszczenia nauki zostały zamieszczone filmy instruktażowe, które poprowadzą użytkownika przez cały kurs krok po kroku.

Wszystkie osoby zainteresowane prywatną nauką programowania(zarówno dzieci, młodzież i studentów), zapraszam do kontaktu za pośrednictwem maila nauczyciel.programowania@gmail.com.
Oferuję także naukę następujących języków programowania: Ansi C, C++, Python, PHP.

--------------------------------------------------
Wprowadzenie do POL++:
- link video

Program nauczania podstaw programowania w POL++:

1. Pierwszy program:
- tworzenie pierwszego programu
- opis struktury pliku programu
- wyświetlanie informacji na konsoli
- link video

2. Zmienne i operacje na nich:
- typy zmiennych
- pobieranie informacji z klawiatury
- operacje arytmetyczne na zmiennych
- liczby pseudolosowe
- link video

3. Instrukcje warunkowe:
- czym są warunki w programowaniu
- rodzaje warunków i ich łączenie
- link video

4. Tablice:
- tablice jednowymiarowe
- tablice wielowymiarowe
- operacje na tablicach
- link video

5. Pętle:
- czym są pętle
- rodzaje pętli i ich wykorzystanie
- operacje na tablicach za pomocą pętli
- link video

6. Funkcje:
- czym są funkcje
- przesyłanie zmiennych do funkcji
- przeładowanie funkcji
- funkcje zwracające wartość
- przesyłanie tablic do funkcji
- link video

7. Klasy cz.1:
- czym są klasy i do czego służą
- zmienne klasy
- funkcje w klasie
- konstruktor klasy i tworzenie obiektu klasy
- operacje na obiektach klasy
- link video

8. Klasy cz.2:
- pola prywatne i publiczne klasy
- funkcje dostępu do zmiennych prywatnych
- funkcje statyczne
- link video

9. Klasy cz.3:
- klasa dziedzicząca
- klasa abstrakcyjna
- link video

10. Praca z plikami i tekstem
- wczytywanie danych z pliku
- zapisywanie danych do pliku
- operacje na zmiennych tekstowych
- link video