/*
translator definiuje nowe nazwy dla operatorow
wymaga kompilatora C++14
*/
#define program int
#define start main

//instrukcje wejscia i wyjscia
#define wprowadz(a) std::cin>>a
void wyswietl() {}
template<typename Pierwszy, typename ... Strings>
void wyswietl(Pierwszy argument, const Strings&... reszta) {
    std::cout<<argument;
    wyswietl(reszta...);
}

//typy danych
#define lbz int
#define lbp double
#define nap std::string
#define znk char
#define log bool

//instrukcje warunkowe
#define jesli if
#define albo else
#define oraz &&
#define lub ||
#define sprawdz switch
#define zawiera case
#define przerwij break
#define przeskocz continue

//petle
#define dla for
#define gdy while
#define wykonuj do

//funkcje
#define funkcja void
#define funkcjalbz int
#define funkcjalbp double
#define funkcjanap std::string
#define funkcjalog bool
#define funkcjaznk char
#define zwroc return

//klasy
#define klasa struct
#define prywatne private
#define dziedziczone protected
#define publiczne public
#define obiekt this
#define nowy new
#define usun delete

//pozostale
#define generujLosowanie() srand(time(NULL))
#define losuj(a, b) rand()%(b+1-a)+a
#define dlugosc length //dla dlugosci stringa



